﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQL9997861.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MySQL9997861
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            names.ItemsSource = loadAllTheData();
        }

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);

            userAccount.Text = b[0];
            fname.Text = b[1];
            lname.Text = b[2];          
            dob.Text = b[3];
            str_number.Text = b[4];
            str_name.Text = b[5];
            postcode.Text = b[6];
            city.Text = b[7];
            phone1.Text = b[8];
            phone2.Text = b[9];
            email.Text = b[10];
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        void clearAll()
        {
            fname.Text = "";
            lname.Text = "";
            dob.Text = "";
            userAccount.Text = "";
            str_number.Text = "";
            str_name.Text = "";
            postcode.Text = "";
            city.Text = "";
            phone1.Text = "";
            phone2.Text = "";
            email.Text = "";
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Checking value";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }


        // ***************************** Object Methods *****************************

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = !Splitter.IsPaneOpen;
        }

        // ***************************** Identification methods for Button controls *****************************
        private async void idButAdd_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {

                var printMessage = "You cannot add a user to the database with empty fields, Please check all fields are filled and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                //Add Information to the Database
                MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }


        private async void idButUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || userAccount.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {
                var printMessage = "To update user data, you must first select a user and then alter data. All fields are required to be completed before you can update to the database.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();

            }
   
            else
            {
                //Update Information in the Database
                MySQLCustom.UpdateData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text, userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private async void idButRemove_Click(object sender, RoutedEventArgs e)
        {
            if (userAccount.Text == "")
            {

                var printMessage = "You must select a user first, before you can select delete data. Please Select a user and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }
        // ***************************** Address methods for Button controls *****************************
        private async void adrsButAdd_Click(object sender, RoutedEventArgs e)
        {

            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {

                var printMessage = "You cannot add a user to the database with empty fields, Please check all fields are filled and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                //Add Information to the Database
                MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private async void aButUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {

                var printMessage = "To update user data, you must first select a user and then alter data. All fields are required to be completed before you can update to the database.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                //Update Information in the Database
                MySQLCustom.UpdateData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text, userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private async void adrsButRemove_Click(object sender, RoutedEventArgs e)
        {
            if (userAccount.Text == "")
            {

                var printMessage = "You must select a user first, before you can select delete data. Please Select a user and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }
        // ***************************** Contact methods for Button controls *****************************
        private async void contButAdd_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {

                var printMessage = "You cannot add a user to the database with empty fields, Please check all fields are filled and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                //Add Information to the Database
                MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private async void contButUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {

                var printMessage = "To update user data, you must first select a user and then alter data. All fields are required to be completed before you can update to the database.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                //Update Information in the Database
                MySQLCustom.UpdateData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text, userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private async void contButRemove_Click(object sender, RoutedEventArgs e)
        {
            if (userAccount.Text == "")
            {

                var printMessage = "You must select a user first, before you can select delete data. Please Select a user and try again.";


                var dialog = new MessageDialog($"{printMessage}");

                dialog.Title = "Oh No! An error has occurred!";

                dialog.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
        }

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }
     /*   private void fill_Click(object sender, RoutedEventArgs e) 
         
            fname.Text = "doug";
            lname.Text = "jackson";
            dob.Text = "12/12/2012";
            str_number.Text = "12";
            str_name.Text = "roadroad"; 
            postcode.Text = "1234";
            city.Text = "tauranga";
            phone1.Text = "1234567";
            phone2.Text = "1234567";
            email.Text = "coolguy767@myspace.com";
        }   */

    }
}